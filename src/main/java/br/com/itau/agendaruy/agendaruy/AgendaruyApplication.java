package br.com.itau.agendaruy.agendaruy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgendaruyApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgendaruyApplication.class, args);
	}

}
